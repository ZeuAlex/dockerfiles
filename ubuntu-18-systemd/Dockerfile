#=====================================================================================
# Ubuntu 18.04 x86_64 - Systemd / Rsyslog / OpenSSH / Sudo
#=====================================================================================
# Build:
# docker build --rm -t kitkatboy/ubuntu-18-systemd ./ubuntu-18-systemd
#
# Run:
# docker run \
#     --detach \
#     --privileged \
#     --name "ubuntu-18-systemd" \
#     --hostname "ubuntu-18-systemd" \
#     -p <container_ssh_exposition_port>:22 \
#     -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
#     kitkatboy/ubuntu-18-systemd
#=====================================================================================
FROM ubuntu:18.04

# Make systemd aware of being run in container
ENV container docker

# Make Ansible aware of being run in container
RUN echo '#!bin/sh \n\n\
export container=docker' > /etc/profile.d/docker.sh


#-------------------------------------------------------------------------------------
# Systemd
#-------------------------------------------------------------------------------------
# Don't start useless services
RUN find /etc/systemd/system \
    /lib/systemd/system \
    -path '*.wants/*' \
    -not -name '*journald*' \
    -not -name '*systemd-tmpfiles*' \
    -not -name '*systemd-user-sessions*' \
    -exec rm \{} \;

VOLUME ["/sys/fs/cgroup"]


#-------------------------------------------------------------------------------------
# Services
#-------------------------------------------------------------------------------------
RUN apt-get update && apt-get -y install \
    systemd \
    dbus \
    rsyslog \
    openssh-server \
    sudo \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Don't boot with default graphical.target
RUN systemctl set-default multi-user.target

# Configure ssh access
RUN sed -i 's/.*PasswordAuthentication .*/PasswordAuthentication no/' /etc/ssh/sshd_config; \
    sed -i 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config;


#-------------------------------------------------------------------------------------
# Users
#-------------------------------------------------------------------------------------
RUN for OPS in arota; \
    do \
        adduser ${OPS} --gecos "First Last,RoomNumber,WorkPhone,HomePhone" --disabled-password; \
        mkdir -m 700 /home/${OPS}/.ssh; \
        wget -q -O - https://bitbucket.org/ZeuAlex/ssh_public_keys/raw/master/${OPS}_id_rsa.pub > /home/${OPS}/.ssh/authorized_keys; \
        chown -R ${OPS}:${OPS} /home/${OPS}; \
        echo "${OPS} ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/ops_users; \
        chmod 440 /etc/sudoers.d/ops_users; \
    done


EXPOSE 22

CMD ["/sbin/init"]
