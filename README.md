Dockerfiles repository
======================

This repository contains Dockerfiles to build images.

Generated builds are available in [Docker cloud](https://cloud.docker.com).


Build
-----

To build and run an image, follow commands indicated in the dockerfile.


Images
------

###### Docker for Ansible

Docker images with SystemD and common tools enabled for Ansible deployment testing.

- Ubuntu 18.04 (Bionic Beaver)
- CentOS 7.5